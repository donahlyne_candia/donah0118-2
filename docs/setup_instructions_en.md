# Setup Instructions #
*(for Linux / Ubuntu only)*

## Requirements: ##
- Python 2.7

Everything was tested on Ubuntu 16.04 LTS and will most probably work on all other Debian based Linux distributions. The scripts itself should run also on other Linux distributions, Mac and Windows, but the setup instructions might slightly deviate from the following steps.

To install this complex project on one or multiple servers, we use the fabric framework. It is run from a local work station and uses SSH to connect to the target servers to install and configure all needed components there. 

### Setup Instructions (local work station)###
1. Fix potential problems with locale (if logging in from a non English / US system):

	> `sudo apt-get install language-pack-id`

	> `sudo dpkg-reconfigure locales`

2. Change to the directory of the project (where this file resides)

3. Start the installation script, answer prompts if there are any:

	> `./install_fabric.sh`

### Setup Instructions (for the target servers, using fabric) ###

1. Change to the directory of the project (where this file resides)

2. Adjust the settings in the stage configuration `deploy/stages/production.py` (according to the comments you find there)

The most important settings in `production.py` are the settings of the target server addresses and login account. Please make sure that:

* it is possible to log in to the servers via SSH either through user+password (in `production.py`) or user + SSH key (loaded on the work station)
* the SSH account used by fabric either can sudo or is root 

3. Start the installation ```fab production deploy```

Once installation completed without errors the server(s) are ready for use.
