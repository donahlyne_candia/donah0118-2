#!/bin/bash

LOCKDIR=/tmp/scrapy_runner_example.lock

if mkdir $LOCKDIR
then
	while true
	do
	    echo "Starting Example Crawler"
	    sleep 30

	    source /usr/local/web/shared/venv/bin/activate
	    cd /usr/local/web/current/scrapers/example
	    scrapy crawl spider_name -o /usr/local/web/shared/working_directories/download_results/$(date "+%b_%d_%Y_%H_%M_%S").csv -a files_dir=/usr/local/project/shared/working_directories/upload_input &>> /var/log/web_ui/example.log
	    echo "Restarting Example Crawler"
	done

	if rmdir $LOCKDIR
    then
        echo "Finished crawler"
    else
        echo "Could not remove lock dir" >&2
    fi
else
    echo "Crawler already running"
fi
