# for number crunching
numpy
pandas
xlrd
docx2txt

# for scraping
scrapy
lxml

selenium
pyvirtualdisplay

# for API / web ui
requests    >= 2.10.0
gunicorn    >= 19.5.0
flask
flask-admin

# Forms.
Flask-WTF==0.9.5
WTForms-Components==0.9.7

# for deployment
# this is better (python 3 compatible), but has no fabtools!!
fabric3
#fabric
#fabtools
#fake

# for testing
scrapy_tdd
pytest
pytest-bdd
pytest-cov
pytest-describe
#pytest-twisted
pytest-watch

#pytest-flask
pytest-xdist

# for database access
psycopg2
#MySQL-python
dataset
sqlalchemy
couchdb


# for python 2.7, otherwise remove the version
ftfy==4.4.3

# various parsing helpers
phonenumbers
nameparser
dateparser

pylint

psutil
