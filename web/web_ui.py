# -*- coding: utf-8 -*-
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../'))
import logging
import json
import logging
from logging.handlers import RotatingFileHandler
import datetime
from flask import Flask, request, jsonify

try:
    from web_settings import project_name
except:
    logging.warning("No Web UI Settings Found. Default: Admin")
    project_name = "Admin"

from libs.views import upload_download_view, error_view, google_web_view
from libs import tools


def scraping_logs_path(app, scraper_name):
    return os.path.join(app.config['LOG_DIR'], '%s_scraping_logs.json' % scraper_name)

def create_app(settings_override=None):
    app = Flask(__name__, instance_relative_config=False)

    # app.config.from_object('config.settings')
    # app.config.from_pyfile('settings.py', silent=False)
    app.config.from_pyfile('web_settings.py', silent=False)

    if settings_override:
        app.config.update(settings_override)

    app.logger.setLevel(app.config['LOG_LEVEL'])

    error_view(app)
    tools.exception_handler(app, project_name)
    if app.config.get('WEB_TYPE') == app.config.get('UPLOAD'):
        upload_download_view(app, project_name)
    else:
        google_web_view(app)

    @app.context_processor
    def project():
        return dict(project_name=project_name)

    @app.route("/progress", methods=["GET"])
    def progress_streamer():
        data = {}
        scraper_name = request.values.get('type')

        if 'example_spider' in request.values.get('type'):
            scraper_name = "spider_name"

        try:
            data = json.load(open(scraping_logs_path(app, scraper_name)))
        except:
            pass
        return jsonify(data)

    @app.route("/stop_crawling_example_spider", methods=["POST"])
    def stop_crawling_spider():
        data = tools.stop_crawling('example_spider', scraping_logs_path(app, "example_spider"))
        return jsonify(data)

    return app


app = create_app()

if __name__ == "__main__":
    app.run()